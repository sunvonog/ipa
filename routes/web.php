<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Startseite der Applikation
Route::get('/', 'HomeController@index')->name('home');

//Login Routes
Auth::routes([
	'reset' => false,
	'verify' => false,
	'register' => false
]);

Route::middleware('auth')->group(function() {

	//Mitarbeiter, Stationsleitung und Admin
	Route::middleware('role:2,3,4')->group(function() {
		//Tag auswählen, anschauen
		Route::get('/days', 'DayController@index')->name('days.index');
		Route::get('/{day}', 'DayController@show')->name('days.show');
		Route::post('/days/get', 'DayController@get')->name('days.get');
		
	});

	//Stationsleitung und Admin
	Route::middleware('role:3,4')->group(function() {
		//Benutzerverwaltung
		Route::get('/users/index', 'UserController@index')->name('users.index');
		Route::get('/users/{user}/edit', 'UserController@edit')->name('users.edit');
		Route::patch('/users/{user}/update', 'UserController@update')->name('users.update');

		//Tag erstellen, bearbeiten, löschen
		Route::get('/days/create', 'DayController@create')->name('days.create');
		Route::post('/days', 'DayController@store')->name('days.store');
		Route::get('/{day}/edit', 'DayController@edit')->name('days.edit');
		Route::patch('/{day}/update', 'DayController@update')->name('days.update');
		Route::delete('/{day}/delete', 'DayController@destroy')->name('days.destroy');

		//Jahr erstellen
		Route::post('/years', 'YearController')->name('years.store');

		//Fall erstellen, bearbeiten, löschen
		Route::get('/{day}/cases/create', 'PatientCaseController@create')->name('cases.create');
		Route::post('/{day}/cases', 'PatientCaseController@store')->name('cases.store');
		Route::get('/{day}/{case}/edit', 'PatientCaseController@edit')->name('cases.edit');
		Route::patch('/{day}/{case}/update', 'PatientCaseController@update')->name('cases.update');
		Route::delete('/{day}/{case}/delete', 'PatientCaseController@destroy')->name('cases.destroy');

		Route::post('/{day}/{case}/active', 'ActiveCaseController')->name('cases.activate');
		Route::post('/{day}/{case}/signout', 'SignoutCaseController')->name('cases.signout');
		Route::post('/{day}/{case}/waitlist', 'WaitlistCaseController')->name('cases.waitlist');
	});

});




