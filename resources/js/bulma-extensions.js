var bulmaCalendar = require('bulma-calendar/dist/js/bulma-calendar');


// Instantiate
var datepicker = bulmaCalendar.attach('#datepicker', {
	type: 'date',
	displayMode: 'dialog',
	lang: 'de',
	dateFormat: 'DD.MM.YYYY',
	weekStart: 1,
	disabledWeekDays: '0',
	showHeader: false,
	showTodayButton: false,
	closeOnSelect: true,
});
var inlinedatepicker = bulmaCalendar.attach('#datepicker-inline', {
	displayMode: 'inline',
	lang: 'de',
	dateFormat: 'DDMMYYYY',
	weekStart: 1,
	disabledWeekDays: '0',
});
