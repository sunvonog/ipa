@extends('layouts.app')

@section('SiteTitle', 'Login')

@section('content')
    <div class="columns is-marginless is-centered">
        <div class="column is-5">
            <!-- Erstellt ein Card -->
            <div class="card">
                <header class="card-header">
                    <p class="card-header-title">Login</p>
                </header>
                <!-- Login Form -->
                <div class="card-content">
                    <form class="login-form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Benutzername</label>
                            </div>

                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <input class="input" id="username" type="text" name="username"
                                               value="{{ old('username') }}" required autofocus>
                                    </p>
                                    @if ($errors->has('username'))
                                        <p class="help is-danger">
                                            {{ $errors->first('username') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Password</label>
                            </div>

                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <input class="input" id="password" type="password" name="password" required>
                                    </p>

                                    @if ($errors->has('password'))
                                        <p class="help is-danger">
                                            {{ $errors->first('password') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label"></div>

                            <div class="field-body">
                                <div class="field">
                                    <p class="control">
                                        <input id="remember" type="checkbox" class="is-checkradio" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        <label for="remember" style="margin-left: 0px;">Remember Me</label>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="field is-horizontal">
                            <div class="field-label"></div>

                            <div class="field-body">
                                <div class="field is-grouped">
                                    <div class="control">
                                        <button type="submit" class="button is-success">Login</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
