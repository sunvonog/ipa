@extends('layouts.app')

@section('SiteTitle', 'Fall bearbeiten')

@section('content')
<div class="columns is-marginless is-centered">
	<div class="column is-7">
		<div class="card">
			<div class="card-header">
				<p class="card-header-title">Fall bearbeiten</p>
			</div>
			<div class="card-content">
				<!-- Ruft das Vue.js Element edit-case auf -->
				<edit-case 
				base="{{ url('/') }}" 
				action="{{ route('cases.update', ['day' => $day->slug, 'case' => $case->id]) }}" 
				day="{{ $day->slug }}" 
				:patient="{{ $patient }}" 
				:case="{{ $case }}">
				</edit-case>
			</div>
		</div>
	</div>
</div>

@endsection