@extends('layouts.app')

@section('SiteTitle', 'Fall erstellen')

@section('content')
<div class="columns is-marginless is-centered">
	<div class="column is-7">
		<div class="card">
			<div class="card-header">
				<p class="card-header-title">Fall erstellen</p>
			</div>
			<div class="card-content">
				<!-- Ruft das Vue.js Element create-case auf -->
				<create-case base="{{ url('/') }}" action="{{ route('cases.store', ['day' => $day->slug]) }}" day="{{ $day->slug }}"></create-patient>
			</div>
		</div>
	</div>
</div>

@endsection