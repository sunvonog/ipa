@extends('layouts.app')

@section('SiteTitle', str_before($day->name, ' '))

@section('content')
<div class="columns is-marginless is-centered">
	<div class="column is-11">
		<div class="field">
			<div class="card">
				<div class="card-header">
					<p class="card-header-title">Aktive Liste</p>
				</div>
				<div class="card-content">
					<table class="table is-fullwidth">
						<thead>
							<tr>
								<th>Zeit</th>
								<th>Name</th>
								<th>Vorname</th>
								<th>Geschlecht</th>
								<th><abbr title="Geburtstagsdatum">Geb. Datum</abbr></th>
								<th><abbr title="Klasse">Kl.</abbr></th>
								<th><abbr title="Abteilung">Abt.</abbr></th>
								<th>Arzt</th>
								<th><abbr title="Intervention">Int.</abbr></th>
								<th><abbr title="geplante Tage">gepl. Tage</abbr></th>
								<th><abbr title="spezielles/Untersuchung">spez.</abbr></th>
								<th><abbr title="Abrechnungen">Abr.</abbr></th>
								<th>Zimmer</th>
								<th><abbr title="Verordnung">Ver.</abbr></th>
								<th><abbr title="Meona Kurve angelegt">Kurve ang.</abbr></th>
								@if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Stationsleitung')
								<th>Aktion</th>
								@endif
							</tr>
						</thead>
						<tbody>
							@foreach($activeBeds as $activeBed)
							<tr style="background-color: {{ $activeBed->department->color }}; height: 41px;">
								<td>{{ $activeBed->patientCase->time ?? '' }}</td>
								<td>{{ $activeBed->patientCase->patient->name ?? '' }}</td>
								<td>{{ $activeBed->patientCase->patient->firstname ?? '' }}</td>
								<td>{{ $activeBed->patientCase->patient->sex ?? '' }}</td>
								<td>{{ $activeBed->patientCase->patient->birthdate ?? '' }}</td>
								<td>{{ $activeBed->patientCase->class ?? '' }}</td>
								<td>{{ $activeBed->department->abbreviation ?? '' }}</td>
								<td>{{ $activeBed->patientCase->doctor ?? '' }}</td>
								<td>{{ $activeBed->patientCase->intervention ?? '' }}</td>
								<td>{{ $activeBed->patientCase->planned_days ?? '' }}</td>
								<td>{{ $activeBed->patientCase->special ?? '' }}</td>
								<td>
									@if($activeBed->patientCase->accounting ?? '')
										<i class="far fa-check-circle"></i>
									@endif
								</td>
								<td>{{ $activeBed->patientCase->room ?? '' }}</td>
								<td>
									@if($activeBed->patientCase->regulation ?? '')
										<i class="far fa-check-circle"></i>
									@endif
								</td>
								<td>
									@if($activeBed->patientCase->meona_curve ?? '')
										<i class="far fa-check-circle"></i>
									@endif
								</td>
								@if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Stationsleitung')
								<td>
								@if($activeBed->patient_case_id != null)
									<div class="buttons">
										<form class="is-inline-block" style="margin-right: 0.5rem"
											action="{{ route('cases.signout', ['day' => $day->slug, 'case' => $activeBed->patientCase->id]) }}"
											method="post">
											@csrf
											<button type="submit" class="button is-info is-small">
												<abbr title="Auf Abmeldeliste setzen"><i class="fas fa-user-alt-slash"></i></abbr>
											</button>
										</form>
										<form class="is-inline-block" style="margin-right: 0.5rem"
											action="{{ route('cases.waitlist', ['day' => $day->slug, 'case' => $activeBed->patientCase->id]) }}"
											method="post">
											@csrf
											<button type="submit" class="button is-info is-small">
												<abbr title="Auf Warteliste setzen"><i class="fas fa-user-clock"></i></abbr>
											</button>
										</form>
										<a href="{{ route('cases.edit', ['day' => $day->slug,'case' => $activeBed->patientCase->id]) }}" class="button is-info is-small">
											<abbr title="Bearbeiten"><i class="fas fa-edit"></i></abbr>
										</a>
										<form class="is-inline-block"
											action="{{ route('cases.destroy', ['day' => $day->slug, 'case' => $activeBed->patientCase->id]) }}" 
											method="post">
											@csrf
											@method('DELETE')
											<button type="submit" onclick="return confirm('Datensatz wirklich löschen?')" name="delete" class="button is-small is-danger">
												<abbr title="Löschen"><i class="fas fa-trash"></i></abbr>
											</button>
										</form>
									</div>
								@endif
								</td>
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@if($waitlistBeds->isNotEmpty())
		<hr>
		<div class="field">
			<div class="card">
				<div class="card-header">
					<p class="card-header-title">Warteliste</p>
				</div>
				<div class="card-content">
					<table class="table is-fullwidth">
						<thead>
							<tr>
								<th>Zeit</th>
								<th>Name</th>
								<th>Vorname</th>
								<th>Geschlecht</th>
								<th><abbr title="Geburtstagsdatum">Geb. Datum</abbr></th>
								<th><abbr title="Klasse">Kl.</abbr></th>
								<th><abbr title="Abteilung">Abt.</abbr></th>
								<th>Arzt</th>
								<th><abbr title="Intervention">Int.</abbr></th>
								<th><abbr title="geplante Tage">gepl. Tage</abbr></th>
								<th><abbr title="spezielles/Untersuchung">spez.</abbr></th>
								<th><abbr title="Abrechnungen">Abr.</abbr></th>
								<th>Zimmer</th>
								<th><abbr title="Verordnung">Ver.</abbr></th>
								<th><abbr title="Meona Kurve angelegt">Kurve ang.</abbr></th>
								@if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Stationsleitung')
								<th>Aktion</th>
								@endif
							</tr>
						</thead>
						<tbody>
							@foreach($waitlistBeds as $waitlistBed)
							<tr style="background-color: {{ $waitlistBed->department->color }}">
								<td>{{ $waitlistBed->patientCase->time ?? '' }}</td>
								<td>{{ $waitlistBed->patientCase->patient->name ?? '' }}</td>
								<td>{{ $waitlistBed->patientCase->patient->firstname ?? '' }}</td>
								<td>{{ $waitlistBed->patientCase->patient->sex ?? '' }}</td>
								<td>{{ $waitlistBed->patientCase->patient->birthdate ?? '' }}</td>
								<td>{{ $waitlistBed->patientCase->class ?? '' }}</td>
								<td>{{ $waitlistBed->department->abbreviation ?? '' }}</td>
								<td>{{ $waitlistBed->patientCase->doctor ?? '' }}</td>
								<td>{{ $waitlistBed->patientCase->intervention ?? '' }}</td>
								<td>{{ $waitlistBed->patientCase->planned_days ?? '' }}</td>
								<td>{{ $waitlistBed->patientCase->special ?? '' }}</td>
								<td>
									@if($waitlistBed->patientCase->accounting ?? '')
										<i class="far fa-check-circle"></i>
									@endif
								</td>
								<td>{{ $waitlistBed->patientCase->room ?? '' }}</td>
								<td>
									@if($waitlistBed->patientCase->regulation ?? '')
										<i class="far fa-check-circle"></i>
									@endif
								</td>
								<td>
									@if($waitlistBed->patientCase->meona_curve ?? '')
										<i class="far fa-check-circle"></i>
									@endif
								</td>
								@if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Stationsleitung')
								<td>
								@if($waitlistBed->patient_case_id != 0)
									<div class="buttons">
										<form class="is-inline-block" style="margin-right: 0.5rem"
											action="{{ route('cases.activate', ['day' => $day->slug, 'case' => $waitlistBed->patientCase->id]) }}"
											method="post">
											@csrf
											<button type="submit" class="button is-info is-small">
												<abbr title="Aktiv setzen"><i class="fas fa-user-check"></i></abbr>
											</button>
										</form>
										<a href="{{ route('cases.edit', ['day' => $day->slug,'case' => $waitlistBed->patientCase->id]) }}" class="button is-info is-small">
											<abbr title="Bearbeiten"><i class="fas fa-edit"></i></abbr>
										</a>
										<form class="is-inline-block"
											action="{{ route('cases.destroy', ['day' => $day->slug, 'case' => $waitlistBed->patientCase->id]) }}" 
											method="post">
											@csrf
											@method('DELETE')
											<button type="submit" onclick="return confirm('Datensatz wirklich löschen?')" name="delete" class="button is-small is-danger">
												<abbr title="Löschen"><i class="fas fa-trash"></i></abbr>
											</button>
										</form>
									</div>
								@endif
								</td>
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@endif
		@if($signoutlistBeds->isNotEmpty())
		<hr>
		<div class="field">
			<div class="card">
				<div class="card-head">
					<p class="card-header-title">Abmeldeliste</p>
				</div>
				<div class="card-content">
					<table class="table is-fullwidth">
						<thead>
							<tr>
								<th>Zeit</th>
								<th>Name</th>
								<th>Vorname</th>
								<th>Geschlecht</th>
								<th><abbr title="Geburtstagsdatum">Geb. Datum</abbr></th>
								<th><abbr title="Klasse">Kl.</abbr></th>
								<th><abbr title="Abteilung">Abt.</abbr></th>
								<th>Arzt</th>
								<th><abbr title="Intervention">Int.</abbr></th>
								<th><abbr title="geplante Tage">gepl. Tage</abbr></th>
								<th><abbr title="spezielles/Untersuchung">spez.</abbr></th>
								<th><abbr title="Abrechnungen">Abr.</abbr></th>
								<th>Zimmer</th>
								<th><abbr title="Verordnung">Ver.</abbr></th>
								<th><abbr title="Meona Kurve angelegt">Kurve ang.</abbr></th>
								@if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Stationsleitung')
								<th>Aktion</th>
								@endif
							</tr>
						</thead>
						<tbody>
							@foreach($signoutlistBeds as $signoutlistBed)
							<tr style="background-color: {{ $signoutlistBed->department->color }}">
								<td>{{ $signoutlistBed->patientCase->time ?? '' }}</td>
								<td>{{ $signoutlistBed->patientCase->patient->name ?? '' }}</td>
								<td>{{ $signoutlistBed->patientCase->patient->firstname ?? '' }}</td>
								<td>{{ $signoutlistBed->patientCase->patient->sex ?? '' }}</td>
								<td>{{ $signoutlistBed->patientCase->patient->birthdate ?? '' }}</td>
								<td>{{ $signoutlistBed->patientCase->class ?? '' }}</td>
								<td>{{ $signoutlistBed->department->abbreviation ?? '' }}</td>
								<td>{{ $signoutlistBed->patientCase->doctor ?? '' }}</td>
								<td>{{ $signoutlistBed->patientCase->intervention ?? '' }}</td>
								<td>{{ $signoutlistBed->patientCase->planned_days ?? '' }}</td>
								<td>{{ $signoutlistBed->patientCase->special ?? '' }}</td>
								<td>
									@if($signoutlistBed->patientCase->accounting ?? '')
										<i class="far fa-check-circle"></i>
									@endif
								</td>
								<td>{{ $signoutlistBed->patientCase->room ?? '' }}</td>
								<td>
									@if($signoutlistBed->patientCase->regulation ?? '')
										<i class="far fa-check-circle"></i>
									@endif
								</td>
								<td>
									@if($signoutlistBed->patientCase->meona_curve ?? '')
										<i class="far fa-check-circle"></i>
									@endif
								</td>
								@if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Stationsleitung')
								<td>
								@if($signoutlistBed->patient_case_id != 0)
									<div class="buttons">
										<form class="is-inline-block" style="margin-right: 0.5rem"
											action="{{ route('cases.activate', ['day' => $day->slug, 'case' => $signoutlistBed->patientCase->id]) }}"
											method="post">
											@csrf
											<button type="submit" class="button is-info is-small">
												<abbr title="Aktiv setzen"><i class="fas fa-user-check"></i></abbr>
											</button>
										</form>
										<a href="{{ route('cases.edit', ['day' => $day->slug,'case' => $signoutlistBed->patientCase->id]) }}" class="button is-info is-small">
											<abbr title="Bearbeiten"><i class="fas fa-edit"></i></abbr>
										</a>
										<form class="is-inline-block"
											action="{{ route('cases.destroy', ['day' => $day->slug, 'case' => $signoutlistBed->patientCase->id]) }}" 
											method="post">
											@csrf
											@method('DELETE')
											<button type="submit" onclick="return confirm('Datensatz wirklich löschen?')" name="delete" class="button is-small is-danger">
												<abbr title="Löschen"><i class="fas fa-trash"></i></abbr>
											</button>
										</form>
									</div>
								@endif
								</td>
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		@endif
	</div>
</div>
@if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Stationsleitung')
<nav class="navbar is-fixed-bottom has-shadow">
	<div class="navbar-center">
		<div class="navbar-item">
			<div class="buttons">
				<a href="{{ route('cases.create', ['day' => $day->slug]) }}" class="button is-info">Fall anlegen</a>
				<a href="{{ route('days.edit', ['day' => $day->slug]) }}" class="button is-primary">Tag bearbeiten</a>
				<form class="is-inline-block" action="{{ route('days.destroy', ['day' => $day->slug]) }}" method="post">
					@csrf
					@method('DELETE')
					<button type="submit" onclick="return confirm('Datensatz wirklich löschen?')" class="button is-danger">Tag löschen</button>
				</form>
			</div>
		</div>
	</div>
</nav>
@endif

@endsection