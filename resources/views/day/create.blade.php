@extends('layouts.app')

@section('SiteTitle', 'Tage erstellen')

@section('content')
<div class="columns is-marginless is-centered">
	<div class="column is-4">
		<div class="card">
			<div class="card-header">
				<p class="card-header-title">Tag erstellen</p>
			</div>
			<div class="card-content">
				<!-- Ruft das Vue.js Element create-day auf -->
				<create-day base="{{ url('/') }}" action="{{ route('days.store') }}" dateerrors="{{ $errors->first('date') ?? '' }}"></create-day>
			</div>
		</div>
	</div>
	<div class="column is-4">
		<div class="card">
			<div class="card-header">
				<p class="card-header-title">Jahr erstellen</p>
			</div>
			<!-- Ruft das Vue.js Element create-year auf -->
			<div class="card-content">
				<create-year base="{{ url('/') }}" action="{{ route('years.store') }}" yearnow="{{ $year }}"></create-year>	
			</div>
		</div>
	</div>
</div>

@endsection