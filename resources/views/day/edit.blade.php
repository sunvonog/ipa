@extends('layouts.app')

@section('SiteTitle', str_before($day->name, ' ').' bearbeiten')

@section('content')
<div class="columns is-marginless is-centered">
	<div class="column is-6">
		<div class="card">
			<div class="card-header">
				<p class="card-header-title">Tag bearbeiten</p>
			</div>
			<div class="card-content">
				<!-- Ruft das Vue.js Element edit-day auf -->
				<edit-day 
					base="{{ url('/') }}" 
					action="{{ route('days.update', ['day' => $day->slug]) }}"
					:day="{{ $day }}"
					:angiologie_value="{{ $angiologieValue }}"
					:kardiologie_value="{{ $kardiologieValue }}"
					:nephrologie_value="{{ $nephrologieValue }}"
					:onkologie_value="{{ $onkologieValue }}"
					:pneumologie_value="{{ $pneumologieValue }}"
					:pneumoschläfer_value="{{ $pneumoschläferValue }}">
				</edit-day>

			</div>
		</div>
	</div>
</div>

@endsection