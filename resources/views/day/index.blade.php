@extends('layouts.app')

@section('SiteTitle', 'Tag auswählen')

@section('content')
<div class="columns is-marginless is-centered">
	<div class="column is-5">
		<div class="card">
			<div class="card-header">
				<p class="card-header-title">Tag auswählen</p>
			</div>
			<div class="card-content">
				<div class="columns is-centered">
					<div class="column is-5">
						<form action="{{ route('days.get') }}" method="post">
							@csrf
							<div class="field">
								<!-- Erstellt einen Datepicker -->
								<div class="control">
									<input type="date" id="datepicker-inline" name="date" class="input">
								</div>
								@if($errors->has('date'))
								<p class="help is-danger">
									{{ $errors->first('date') }}
								</p>
								@endif
							</div>
							<div class="field">
								<div class="control">
									<input type="submit" name="submit" class="button is-success">
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection