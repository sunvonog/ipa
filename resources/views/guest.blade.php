@extends('layouts.app')

@section('SiteTitle', 'Keine Berechtigung')

@section('content')
    <div class="container">
        <div class="columns is-marginless is-centered">
            <div class="column is-7">
                <nav class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            Keine Berechtigung um die Seite zu sehen
                        </p>
                    </header>

                    <div class="card-content">
                        Bitte melden Sie sich bei der Stationsleitung der KUK.
                    </div>
                </nav>
            </div>
        </div>
    </div>
@endsection
