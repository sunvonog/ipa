<!-- Öffnet eine Notification Box -->
@if (session('success'))
<section class="section">
	<div class="container">
	    <div class="notification is-success">
	        <button class="delete"></button>
	        {{ session('success') }}
	    </div>
	</div>
</section>
@endif

<!-- Öffnet eine Notification Box -->
@if (session('error'))
<div class="section">
	<div class="container">
	    <div class="notification is-danger">
	        <button class="delete"></button>
	        {{ session('error') }}
	    </div>
	</div>
</div>
@endif