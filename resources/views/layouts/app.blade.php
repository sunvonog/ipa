<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" @if(\Route::current()->getName() == 'days.show') class="has-navbar-fixed-bottom" @endif>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Webapplikation Titel wird gesetzt -->
        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Styles -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style type="text/css">
            abbr[title] {
              border-bottom: none !important;
              cursor: inherit !important;
              text-decoration: none !important;
            }
            .navbar-center {
                margin-left: auto;
                margin-right: auto;
            }
        </style>
    </head>
    <body>
        <div id="app">
            <!-- Navbar wird erstellt -->
            <nav class="navbar has-shadow" role="navigation" aria-label="main navigation">
                <div class="container">
                    <div class="navbar-brand">
                        <a href="{{ route('home') }}" class="navbar-item">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/f/f2/Logo_Universitätsspital_Basel.svg" style="margin-right: 1em;">
                        </a>

                        <div class="navbar-burger burger" data-target="navMenu">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                    <div class="navbar-menu" id="navMenu">
                        @if(!Auth::guest())
                        <div class="navbar-start">
                            <a href="{{ route('home') }}" class="navbar-item">
                                Home
                            </a>
                            @if(!Auth::guest() && Auth::user()->role->name != 'Gast')
                            <div class="navbar-item has-dropdown is-hoverable">
                                <a class="navbar-link" href="">Tag</a>
                                <div class="navbar-dropdown">
                                    @if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Stationsleitung')
                                    <a class="navbar-item" href="{{ route('days.create') }}">
                                        Tage erstellen
                                    </a>
                                    @endif
                                    <a class="navbar-item" href="{{ route('days.index') }}">
                                        Tag auswählen
                                    </a>
                                </div>
                            </div>
                            @endif
                            @if(Auth::user()->role->name == 'Admin' || Auth::user()->role->name == 'Stationsleitung')
                            <a href="{{ route('users.index') }}" class="navbar-item">
                                Benutzerverwaltung
                            </a>
                            @endif
                        </div>
                        @endif

                        <div class="navbar-end">
                            @if (Auth::guest())
                                <a class="navbar-item" href="{{ route('login') }}">Login</a>
                            @else
                                <div class="navbar-item has-dropdown is-hoverable">
                                    <a class="navbar-link" href="#">{{ Auth::user()->name }}</a>

                                    <div class="navbar-dropdown">
                                        <a class="navbar-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                              style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </nav>
            <!-- Seiteninhalt der einzelnen Seiten wird hier reingeschrieben -->
            <section class="hero is-danger">
                <div class="hero-body">
                    <div class="container">
                        <h1 class="title">
                            @yield('SiteTitle')
                        </h1>
                    </div>
                </div>
            </section>
            @include('layouts.partials._alert')
            @yield('content')
        </div>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
