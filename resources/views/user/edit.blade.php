@extends('layouts.app')

@section('SiteTitle', 'Benutzer '.$user->name.' bearbeiten')

@section('content')
<div class="columns is-marginless is-centered">
	<div class="column is-5">
		<div class="card">
			<div class="card-header">
				<p class="card-header-title">
					Benutzer bearbeiten
				</p>
			</div>
			<div class="card-content">
				<form action="{{ route('users.update', ['user' => $user->id]) }}" method="POST">
					@csrf
					@method('PATCH')
					<div class="field">
						<label class="label">ID</label>
						<div class="control">
							<input type="text" disabled="" class="input" name="id" value="{{ $user->id }}">
						</div>
					</div>
					<div class="field">
						<label class="label">Name</label>
						<div class="control">
							<input type="text" disabled="" class="input" name="name" value="{{ $user->name }}">
						</div>
					</div>
					<div class="field">
						<label class="label">E-Mail</label>
						<div class="control">
							<input type="text" disabled="" class="input" name="email" value="{{ $user->email }}">
						</div>
					</div>
					<div class="field">
						<label class="label">Abteilung</label>
						<div class="control">
							<input type="text" disabled="" class="input" name="department" value="{{ $user->department }}">
						</div>
					</div>
					<div class="field">
						<label class="label">Rolle</label>
						<div class="select is-fullwidth">
							<select name="role">
								@foreach($roles as $role)
								<option value="{{ $role->id }}" {{ $user->role->id == $role->id ? 'selected' : '' }}>
									{{ $role->name }}
								</option>
								@endforeach
							</select>
						</div>
						@if ($errors->has('role'))
						    <p class="help is-danger">
						        {{ $errors->first('role') }}
						    </p>
						@endif
					</div>
					<div class="field is-grouped">
						<div class="control">
							<button type="submit" class="button is-success">Senden</button>
						</div>
						<div class="control">
							<a href="{{ route('users.index') }}" class="button is-light">Abbrechen</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection