@extends('layouts.app')

@section('SiteTitle', 'Benutzerverwaltung')

@section('content')
<div class="columns is-marginless is-centered">
	<div class="column is-10">
		<div class="card">
			<div class="card-header">
				<p class="card-header-title">
					Benutzerverwaltung
				</p>
			</div>
			<div class="card-content">
				<table class="table is-fullwidth">
					<thead>
						<tr>
							<th>ID</th>
							<th>Nachname</th>
							<th>Vorname</th>
							<th>E-Mail</th>
							<th>Abteilung</th>
							<th>Rolle</th>
							<th>Erstellt am</th>
							<th>Aktion</th>
						</tr>
					</thead>
					<tbody>
						@foreach($users as $user)
						<tr>
							<th>{{ $user->id }}</th>
							<td>{{ $user->lastname }}</td>
							<td>{{ $user->firstname }}</td>
							<td>{{ $user->email }}</td>
							<td>{{ $user->department }}</td>
							<td>{{ $user->role->name }}</td>
							<td>{{ $user->created_at }}</td>
							<td>
								<a href="{{ route('users.edit', ['user' => $user->id]) }}" class="button is-info is-small">Bearbeiten</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

@endsection