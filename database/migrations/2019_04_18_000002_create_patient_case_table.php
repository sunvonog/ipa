<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientCaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_cases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('time');
            $table->enum('class', ['1.', '2.', '3.']);
            $table->bigInteger('department_id')->unsigned();
            $table->string('doctor')->nullable();
            $table->string('intervention')->nullable();
            $table->integer('planned_days')->nullable();
            $table->string('special')->nullable();
            $table->boolean('accounting')->default(0);
            $table->string('room')->nullable();
            $table->boolean('regulation')->default(0);
            $table->boolean('meona_curve')->default(0);
            $table->bigInteger('patient_id')->unsigned();
            $table->timestamps();

            //Erzeugt die Fremdschlüssel
            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_case');
    }
}
