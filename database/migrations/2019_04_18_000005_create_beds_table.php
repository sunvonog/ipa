<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Erzeugt die Attribute in der Tabelle beds
        Schema::create('beds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('day_id')->unsigned();
            $table->bigInteger('patient_case_id')->unsigned()->nullable();
            $table->bigInteger('department_id')->unsigned()->nullable();
            $table->enum('type', ['Aktiv', 'Abmeldung', 'Warteliste'])->default('Aktiv');
            $table->timestamps();

            //Erzeugt die Fremdschlüssel
            $table->foreign('day_id')->references('id')->on('days')->onDelete('cascade');
            $table->foreign('patient_case_id')->references('id')->on('patient_cases')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beds');
    }
}
