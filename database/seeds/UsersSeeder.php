<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/**
    	 * Erstellt mehrere Instanzen von User mit verschiedenen Rollen
    	 *
    	 * Somit kann ich auch ohne AD User auf die Webapplikation zugreifen
    	 */
        App\User::create([
        	'objectguid' => str_random(20),
        	'name' => 'Gast',
        	'username' => 'gast',
        	'email' => 'gast@testmail.com',
        	'password' => bcrypt('password'),
        	'department' => 'Informations-und Kommunikationstechnologie, Ressort',
        	'role_id' => 1,
        ]);

        App\User::create([
        	'objectguid' => str_random(20),
        	'name' => 'Mitarbeiter',
        	'username' => 'mitarbeiter',
        	'email' => 'mitarbeiter@testmail.com',
        	'password' => bcrypt('password'),
        	'department' => 'Medizinische Kurzeitklinik 8.2',
        	'role_id' => 2,
        ]);

        App\User::create([
        	'objectguid' => str_random(20),
        	'name' => 'Stationsleiter',
        	'username' => 'stationsleiter',
        	'email' => 'stationsleiter@testmail.com',
        	'password' => bcrypt('password'),
        	'department' => 'Medizinische Kurzeitklinik 8.2',
        	'role_id' => 3,
        ]);

        App\User::create([
        	'objectguid' => str_random(20),
        	'name' => 'Admin',
        	'username' => 'admin',
        	'email' => 'admin@testmail.com',
        	'password' => bcrypt('password'),
        	'department' => 'Informations-und Kommunikationstechnologie, Ressort',
        	'role_id' => 4,
        ]);

       	/**
       	 * Hier wird eine Instanz von meinem Benutzer erstellt.
       	 *
       	 * Damit habe ich beim ersten einloggen schon die Rolle Admin
       	 */
       	App\User::create([
        	'objectguid' => '3775b28a-7a80-42f5-9356-e463e6cd1d83',
        	'name' => 'Grun Oliver',
        	'username' => 'gruno',
        	'email' => 'oliver.grun@usb.ch',
        	'password' => bcrypt('password'),
        	'department' => 'Informations-und Kommunikationstechnologie, Ressort',
        	'role_id' => 4,
        ]);

    }
}
