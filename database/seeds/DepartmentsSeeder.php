<?php

use Illuminate\Database\Seeder;

class DepartmentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Erstellt mehrere Instanzen von Departement
        App\Department::create([
        	'name' => 'Angiologie',
        	'abbreviation' => 'A',
        	'color' => 'lightgreen',
        ]);

        App\Department::create([
        	'name' => 'Kardiologie',
        	'abbreviation' => 'K',
        	'color' => '#da7cf9',
        ]);

        App\Department::create([
        	'name' => 'Nephrologie',
        	'abbreviation' => 'N',
        	'color' => 'orange',
        ]);

        App\Department::create([
        	'name' => 'Onkologie',
        	'abbreviation' => 'O',
        	'color' => 'lightblue',
        ]);

        App\Department::create([
        	'name' => 'Pneumologie',
        	'abbreviation' => 'P',
        	'color' => 'yellow',
        ]);

        App\Department::create([
        	'name' => 'Pneumologie_Schläfer',
        	'abbreviation' => 'PS',
        	'color' => 'pink',
        ]);

        App\Department::create([
        	'name' => 'Andere',
        	'abbreviation' => '',
        	'color' => '#F3F3F3',
        ]);
    }
}
