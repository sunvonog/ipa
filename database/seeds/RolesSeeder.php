<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Erstellt mehrere Instanzen von Role
        App\Role::create([
        	'name' => 'Gast',
        ]);

        App\Role::create([
        	'name' => 'Mitarbeiter',
        ]);

        App\Role::create([
        	'name' => 'Stationsleitung',
        ]);
        
        App\Role::create([
        	'name' => 'Admin',
        ]);
    }
}
