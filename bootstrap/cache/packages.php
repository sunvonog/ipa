<?php return array (
  'adldap2/adldap2-laravel' => 
  array (
    'providers' => 
    array (
      0 => 'Adldap\\Laravel\\AdldapServiceProvider',
      1 => 'Adldap\\Laravel\\AdldapAuthServiceProvider',
    ),
    'aliases' => 
    array (
      'Adldap' => 'Adldap\\Laravel\\Facades\\Adldap',
    ),
  ),
  'beyondcode/laravel-dump-server' => 
  array (
    'providers' => 
    array (
      0 => 'BeyondCode\\DumpServer\\DumpServerServiceProvider',
    ),
  ),
  'fideloper/proxy' => 
  array (
    'providers' => 
    array (
      0 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    ),
  ),
  'laravel-frontend-presets/bulma' => 
  array (
    'providers' => 
    array (
      0 => 'LaravelFrontendPresets\\BulmaPreset\\BulmaPresetServiceProvider',
    ),
  ),
  'laravel/tinker' => 
  array (
    'providers' => 
    array (
      0 => 'Laravel\\Tinker\\TinkerServiceProvider',
    ),
  ),
  'nesbot/carbon' => 
  array (
    'providers' => 
    array (
      0 => 'Carbon\\Laravel\\ServiceProvider',
    ),
  ),
  'nunomaduro/collision' => 
  array (
    'providers' => 
    array (
      0 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    ),
  ),
);