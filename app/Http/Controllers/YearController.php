<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Day;
use App\Bed;
use Carbon\Carbon;

class YearController extends Controller
{
    /**
     * Erstellt alle Tage für ein Wochentag für ein Jahr
     * @param  Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //Validiert die Benutzereingaben
    	$request->validate([
    		'year' => 'required|string|size:4',
    		'week_day' => 'required',
    		'number_of_beds' => 'required|numeric|min:0|max:50',
    		'angiologie' => 'nullable|numeric|min:0',
    		'kardiologie' => 'nullable|numeric|min:0',
    		'nephrologie' => 'nullable|numeric|min:0',
    		'onkologie' => 'nullable|numeric|min:0',
    		'pneumologie' => 'nullable|numeric|min:0',
    		'pneumoschläfer' => 'nullable|numeric|min:0',
    		'empty' => 'nullable|numeric|min:0',
    	]);

        //Erstellt einen Array von allen Tagen von einem Wochentag in dem Jahr.
        //Funktion mit Hilfe von https://stackoverflow.com/questions/41938018/how-to-get-all-fridays-within-date-range-by-carbon
    	for($date = $this->_getStart($request->year, $request->week_day); $date->lte($this->_getEnd($request->year)); $date->addWeek()) {
    		$weekDay[] = $date->format('d.m.Y');
    	}

        //Erstellt oder Überschreibt alle Tage, die in dem Array sind
    	foreach($weekDay as $helper) {
    		$date = Carbon::create($helper);

    		$day = Day::updateOrCreate(
    			[
    				'name' => $helper.' 00:00',
    				'date' => $date
    			],
    			[
    				'number_of_beds' => $request->number_of_beds,
    			]
    		);

            //Löscht alle Betten von diesem Tag
    		$deletedBeds = Bed::OfDay($day)->delete();

            /*
            Die Funktion _storeBed() wird so oft aufgerufen, wie die eingegebenen Kontingente
            und übergibt die Parameter id des gerade erstellten Days und die id der Abteilung
             */
    		for($i = 0;$i < $request->angiologie;$i++) {
    		    $this->_storeBed($day, 1);
    		}

    		for($i = 0;$i < $request->kardiologie;$i++) {
    		    $this->_storeBed($day, 2);
    		}

    		for($i = 0;$i < $request->nephrologie;$i++) {
    		    $this->_storeBed($day, 3);
    		}

    		for($i = 0;$i < $request->onkologie;$i++) {
    		    $this->_storeBed($day, 4);
    		}

    		for($i = 0;$i < $request->pneumologie;$i++) {
    		    $this->_storeBed($day, 5);
    		}

    		for($i = 0;$i < $request->pneumoschläfer;$i++) {
    		    $this->_storeBed($day, 6);
    		}

    		for($i = 0;$i < $request->empty;$i++) {
    		    $this->_storeBed($day, 7);
    		}
    	}

    	return redirect()->route('days.create')
            ->withSuccess('Tage wurden erfolgreich erstellt');
    }

    /**
     * Helper Funktion um die Betten zu erstellen.
     * Wird von der Store Funktion aufgerufen.
     * @param  int
     * @param  int
     */
    private function _storeBed($day, $department)
    {
   		$day->beds()->create([
   			'department_id' => $department,
   			'type' => 'aktiv',
   		]);
    }

    /**
     * Helper Funktion um das Startdatum herauszufinden.
     * @param  string 
     * @param  string 
     * @return date 
     */
    private function _getStart($year, $day)
    {
        /*
        Wenn das eingebene Jahr das aktuelle Jahr ist,
        ist das Startdatum der gesuchte Wochentag, ab dem heutigen Tag 
         */
    	if($year == Carbon::now()->year) {
    		return Carbon::now()->subDay()->modify('next '.$day);
    	} 

        /*
        Wenn das eingeben Jahr nicht das aktuelle Jahr ist,
        ist das Startdatum der gesuchte Wochentag, ab dem dem 31.12 ein Jahr bevor des eingebenen Jahres
         */
    	$startYear = (string)((int)$year - 1);

    	return Carbon::parse('31.12.'.$startYear)->modify('next '.$day);
    }

    /**
     * Helper Funktion um das Enddatum herauszufinden.
     * @param  string  
     * @param  string 
     * @return date 
     */
    private function _getEnd($year)
    {
    	return Carbon::parse('31.12.'.(string)$year);
    }
}
