<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Day;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Redirected je nach User Rolle auf verschiedene Seiten
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $now = Carbon::now()->format('dmY');
        if(\Auth::user()->role->name != 'Gast' && !\Auth::guest()) {
            if(Day::where('slug', $now)->exists()) {
                return redirect()->route('days.show', ['day' => $now]);
            } 
            if(\Auth::user()->role->name == 'Admin' || \Auth::user()->role->name == 'Stationsleitung') {
                return redirect()->route('days.create');
            }
            return redirect()->route('days.index');
        }

        return view('guest');

    }
}
