<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UserController extends Controller
{

	/**
	 * Ruft die Seite user.index mit allen Usern auf
	 * @return \Illuminate\Http\Response
	 */
    public function index()
    {
    	return view('user.index')
    		->with('users', User::all());
    }

    /**
     * Ruft die Seite user.edit mit dem ausgewählten User und allen Rollen auf
     * @param  User
     * @return \Illuminate\Http\Response       
     */
    public function edit(User $user)
    {
    	return view('user.edit')
    		->with('user', $user)
    		->with('roles', Role::all());
    }

    /**
     * Aktualisiert den editierten User
     * @param  User
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response           
     */
    public function update(User $user, Request $request)
    {
    	$data = $request->validate([
    		'role' => 'required|exists:roles,id',
    	]);

    	$user->update([
    		'role_id' => $request->role
    	]);

    	return redirect()->route('users.index')
    		->withSuccess('Benutzer wurde bearbeitet.');

    }
}
