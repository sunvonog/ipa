<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Day;
use App\Patient;
use App\PatientCase;
use App\Bed;

class PatientCaseController extends Controller
{
    /**
     * Ruft die Seite case.create auf
     * @param  Day
     * @return \Illuminate\Http\Response
     */
    public function create(Day $day)
    {
    	return view('case.create')
    		->with('day', $day);
    }


    /**
     * Erstellt oder wählt einen Patienten aus und erstellt einen neuen Fall
     * Der Fall wird in ein verfügbares Bett gesetzt
     * @param  Request
     * @param  Day
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Day $day)
    {
    	$request->validate([
    		'lastname' => 'required',
    		'firstname' => 'required',
    		'sex' => 'required',
    		'birthdate' => 'required|date',
    		'time' => 'required',
    		'insurance_class' => 'required',
    		'doctor' => 'nullable',
    		'intervention' => 'nullable',
    		'department' => 'required',
    		'planned_days' => 'numeric | nullable',
    		'special' => 'nullable',
    		'room' => 'nullable',
    	]);

    	$patient = Patient::firstOrCreate(
    		['name' => $request->lastname, 'firstname' => $request->firstname, 'birthdate' => $request->birthdate],
    		['sex' => $request->sex]
    	);

    	$case = PatientCase::create([
    		'time' => $request->time,
    		'class' => $request->insurance_class,
    		'department_id' => $request->department,
    		'doctor' => $request->doctor,
    		'intervention' => $request->intervention,
    		'planned_days' => $request->planned_days,
    		'special' => $request->special,
    		'accounting' => $request->has('accounting'),
    		'room' => $request->room,
    		'regulation' => $request->has('regulation'),
    		'meona_curve' => $request->has('meona_curve'),
    		'patient_id' => $patient->id,
    	]);

    	$this->_selectBed($day, $request->department, $case);

    	return redirect()->route('days.show', ['day' => $day->slug])
            ->withSuccess('Patient wurde aufgenommen');
    }

    /**
     * Ruft die Seite case.edit auf
     * @param  Day
     * @param  PatientCase
     * @return \Illuminate\Http\Response
     */
    public function edit(Day $day, PatientCase $case)
    {
    	return view('case.edit')
    		->with('case', $case)
    		->with('day', $day)
    		->with('patient', $case->patient);
    }

    /**
     * Aktualisiert den Fall und den Patienten und setzt das Bett, falls die Abteilung geändert hat in ein neues Bett
     * @param  Day
     * @param  PatientCase
     * @param  Request
     * @return \Illuminate\Http\Response
     */
    public function update(Day $day, PatientCase $case, Request $request)
    {
    	$oldDepartment = $case->department_id;

    	$request->validate([
    		'lastname' => 'required',
    		'firstname' => 'required',
    		'sex' => 'required',
    		'birthdate' => 'required|date',
    		'time' => 'required',
    		'insurance_class' => 'required',
    		'doctor' => 'nullable',
    		'intervention' => 'nullable',
    		'department' => 'required',
    		'planned_days' => 'numeric | nullable',
    		'special' => 'nullable',
    		'room' => 'nullable',
    	]);

    	$patient = $case->patient;

    	$patient->update([
    		'name' => $request->lastname,
    		'firstname' => $request->firstname,
    		'sex' => $request->sex,
    		'birthdate' => $request->birthdate,
    	]);

    	if($oldDepartment != $request->department) {
	    	

	    	if($case->bed->type != 'Aktiv') {
	    		$case->bed->delete();
	    	} else {
	    		$case->bed->update([
	    			'patient_case_id' => null,
	    		]);

	    		$case->bed->save();
	    	}
	    	$this->_selectBed($day, $request->department, $case);
	    }

	    $case->update([
    		'time' => $request->time,
    		'class' => $request->insurance_class,
    		'department_id' => $request->department,
    		'doctor' => $request->doctor,
    		'intervention' => $request->intervention,
    		'planned_days' => $request->planned_days,
    		'special' => $request->special,
    		'accounting' => $request->has('accounting'),
    		'room' => $request->room,
    		'regulation' => $request->has('regulation'),
    		'meona_curve' => $request->has('meona_curve')
    	]);

    	return redirect()->route('days.show', ['day' => $day->slug])
    		->withSuccess('Patient wurde erfolgreich bearbeitet');
    }

    /**
     * Löscht den Fall
     * @param  Day
     * @param  PatientCase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Day $day, PatientCase $case)
    {
        if ($case->bed->type === 'Aktiv') {
            $case->delete();
        } else {
            $case->bed->delete();
            $case->delete();
        }

        return back()
            ->withSuccess('Fall wurde gelöscht');
    }

    /**
     * Wählt das erstbeste, freie, aktive Bett aus, mit der richtigen Abteilung
     * Falls es kein freies, aktives Bett hat, erstellt es ein Bett in der Warteliste
     * und setzt den Fall in das neu erstellte Bett
     * @param  Day
     * @param  int
     * @param  PatientCase
     */
    private function _selectBed($day, $department, $case)
    {    
        $bed = Bed::ofDay($day)->active()->ofDepartment($department)->hasNoPatientCase()->first();
        if($bed != null) {
	        $bed->update([
	        	'patient_case_id' => $case->id,
	        ]);
	        $bed->save();
        } else {
        	Bed::create([
                'day_id' => $day->id,
                'department_id' => $department,
                'patient_case_id' => $case->id,
                'type' => 'warteliste',
            ]);
        }
    }
}
