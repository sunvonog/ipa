<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Day;
use App\PatientCase;
use App\Bed;

class WaitlistCaseController extends Controller
{
    /**
     * Setzt den Fall auf die Warteliste
     * @param  Day
     * @param  PatientCase
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Day $day, PatientCase $case)
    {
    	$bed = $case->bed;
    	$department = $case->department->id;

    	$this->_selectBed($day, $department, $case);

    	$bed->update([
            'patient_case_id' => null,
        ]);
    	return redirect()->route('days.show', ['day' => $day->slug])
    		->withSuccess('Patient wurde erfolgreich auf die Warteliste verschoben');
    }

    /**
     * Wählt das erstbeste, freie, Bett auf der Warteliste aus, mit der richtigen Abteilung
     * Falls es kein freies, aktives Bett hat, erstellt es ein Bett in der Warteliste
     * und setzt den Fall in das neu erstellte Bett
     * @param  Day
     * @param  int
     * @param  PatientCase
     */
    private function _selectBed($day, $department, $case)
    {
    	$bed = Bed::ofDay($day)->waitlist()->ofDepartment($department)->hasNoPatientCase()->first();
        if($bed != null) {
	        $bed->update([
	        	'patient_case_id' => $case->id,
	        ]);
	        $bed->save();
        } else {
        	$day->beds()->create([
                'department_id' => $department,
                'patient_case_id' => $case->id,
                'type' => 'warteliste',
            ]);
        }
    }
}
