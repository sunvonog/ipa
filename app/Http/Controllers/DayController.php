<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Day;
use Carbon\Carbon;
use App\Bed;
use App\Department;

class DayController extends Controller
{
    /**
     * Ruft die Seite day.index auf
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('day.index');
    }

    /**
     * Ruft die Seite day.create auf
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('day.create')
            ->with('year', Carbon::now()->year);
    }

    /**
     * Erstellt einen neuen Tag und ruft die Funktion auf um neue Betten zu erstellen
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Die Benutzereingaben werden Serverseitig validiert
        $request->validate([
            'date' => 'required|unique:days,name',
            'number_of_beds' => 'required|numeric|min:0|max:50',
            'angiologie' => 'nullable|numeric',
            'kardiologie' => 'nullable|numeric',
            'nephrologie' => 'nullable|numeric',
            'onkologie' => 'nullable|numeric',
            'pneumologie' => 'nullable|numeric',
            'pneumoschläfer' => 'nullable|numeric',
            'empty' => 'nullable|numeric|min:0',
        ]);

        //Erstellt ein Datum aus der Benutzereingabe
        $date = Carbon::create($request->date);

        //Eine neuer Tag wird erstellt
        $day = Day::create([
            'name' => $request->date,
            'date' => $date,
            'number_of_beds' => $request->number_of_beds,
        ]);

        /*
        Die Funktion _storeBed() wird so oft aufgerufen, wie die eingegebenen Kontingente
        und übergibt die Parameter id des gerade erstellten Days und die id der Abteilung
         */
        for($i = 0;$i < $request->angiologie;$i++) {
            $this->_storeBed($day, 1);
        }

        for($i = 0;$i < $request->kardiologie;$i++) {
            $this->_storeBed($day, 2);
        }

        for($i = 0;$i < $request->nephrologie;$i++) {
            $this->_storeBed($day, 3);
        }

        for($i = 0;$i < $request->onkologie;$i++) {
            $this->_storeBed($day, 4);
        }

        for($i = 0;$i < $request->pneumologie;$i++) {
            $this->_storeBed($day, 5);
        }

        for($i = 0;$i < $request->pneumoschläfer;$i++) {
            $this->_storeBed($day, 6);
        }

        for($i = 0;$i < $request->empty;$i++) {
            $this->_storeBed($day, 7);
        }

        return redirect()->route('days.create')
            ->withSuccess('Tag wurde erfolgreich erstellt.');

    }

    /**
     * Ruft die Seite day.show auf
     *
     * @param  Day
     * @return \Illuminate\Http\Response
     */
    public function show(Day $day)
    {
        return view('day.show')
            ->with('day', $day)
            ->with('activeBeds', Bed::ofDay($day)->active()->get())
            ->with('waitlistBeds', Bed::ofDay($day)->waitlist()->get())
            ->with('signoutlistBeds', Bed::ofDay($day)->signoutlist()->get());
    }

    /**
     * Ruft die Seite day.edit auf
     *
     * @param  Day
     * @return \Illuminate\Http\Response
     */
    public function edit(Day $day)
    {
        return view('day.edit')
            ->with('day', $day)
            ->with('angiologieValue', Bed::ofDay($day)->active()->ofDepartment(1)->count())
            ->with('kardiologieValue', Bed::ofDay($day)->active()->ofDepartment(2)->count())
            ->with('nephrologieValue', Bed::ofDay($day)->active()->ofDepartment(3)->count())
            ->with('onkologieValue', Bed::ofDay($day)->active()->ofDepartment(4)->count())
            ->with('pneumologieValue', Bed::ofDay($day)->active()->ofDepartment(5)->count())
            ->with('pneumoschläferValue', Bed::ofDay($day)->active()->ofDepartment(6)->count())
            ->with('emptyValue', Bed::ofDay($day)->active()->ofDepartment(7)->count());
    }

    /**
     * Aktualisiert den Tag
     *
     * @param  \Illuminate\Http\Request
     * @param  Day
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Day $day)
    {
        $request->validate([
            'number_of_beds' => 'required|numeric|min:0|max:50',
            'angiologie' => 'nullable|numeric',
            'kardiologie' => 'nullable|numeric',
            'nephrologie' => 'nullable|numeric',
            'onkologie' => 'nullable|numeric',
            'pneumologie' => 'nullable|numeric',
            'pneumoschläfer' => 'nullable|numeric',
            'empty' => 'nullable|numeric|min:0',
        ]);

        for($i = 0;$i < Department::count();$i++) {
            $bett[] = Bed::ofDay($day)->ofDepartment($i+1)->active()->hasPatientCase()->count();
        }

        $id = 0;
        foreach(Department::all() as $department) {
            if($request[strtolower($department->name)] < $bett[$id]) {
                return back()
                    ->withError('Tag konnte nicht bearbeitet werden.');
            }
            $id++;
        }

        $day->update([
            'number_of_beds' => $request->number_of_beds,
        ]);

        $deletedBeds = Bed::ofDay($day)->hasNoPatientCase()->delete();

        for($i = 0;$i < ($request->angiologie - $bett[0]);$i++) {
            $this->_storeBed($day, 1);
        }

        for($i = 0;$i < ($request->kardiologie - $bett[1]);$i++) {
            $this->_storeBed($day, 2);
        }

        for($i = 0;$i < ($request->nephrologie - $bett[2]);$i++) {
            $this->_storeBed($day, 3);
        }

        for($i = 0;$i < ($request->onkologie - $bett[3]);$i++) {
            $this->_storeBed($day, 4);
        }

        for($i = 0;$i < ($request->pneumologie - $bett[4]);$i++) {
            $this->_storeBed($day, 5);
        }

        for($i = 0;$i < ($request->pneumoschläfer - $bett[5]);$i++) {
            $this->_storeBed($day, 6);
        }

        for($i = 0;$i < ($request->empty - $bett[6]);$i++) {
            $this->_storeBed($day, 7);
        }

        return redirect()->route('days.show', ['day' => $day->slug])
            ->withSuccess('Tag wurde erfolgreich bearbeitet');

    }

    /**
     * Löscht den Tag
     *
     * @param  Day
     * @return \Illuminate\Http\Response
     */
    public function destroy(Day $day)
    {
        $day->delete();

        return redirect()->route('home')
            ->withSuccess('Tag wurde erfolgreich gelöscht');
    }

    /**
     * Prüft ob es den ausgewählten Tag gibt und verweist auf die Detailansicht des Tages
     * @param  Request
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $request->validate([
            'date' => 'exists:days,slug'
        ]);

        $day = Day::requestedDay($request->date)->first();

        return redirect()->route('days.show', ['day' => $day->slug]);
    }

    /**
     * Helper Funktion um die Betten zu erstellen.
     * Wird von der Store Funktion aufgerufen.
     * @param  int 
     * @param  int
     */
    private function _storeBed($day, $department)
    {
        $day->beds()->create([
            'department_id' => $department,
            'type' => 'aktiv',
        ]);
    }
}
