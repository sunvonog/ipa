<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Checkt ob der Benutzer der Seite die benötigte Rolle hat.
     * @param  \Illuminate\Http\Request  $request [description]
     * @param  \Closure $next   
     * @param  string  
     * @param  string  
     * @param  string  
     * @return mixed         
     */
    public function handle($request, Closure $next, $role, $role2 = '', $role3 = '')
    {
        if (!$request->user()->hasRole($role) && !$request->user()->hasRole($role2) && !$request->user()->hasRole($role3)) {
            return abort(403);
        }

        return $next($request);
    }
}
