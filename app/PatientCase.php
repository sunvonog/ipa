<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientCase extends Model
{
	/**
	 * Gibt an welche Attribute von Userinputs eingetragen werden dürfen
	 * 
	 * @var array
	 */
    protected $fillable = [
    	'time', 'class', 'department_id', 'doctor', 'intervention', 'planned_days', 'special', 'accounting', 'room', 'regulation', 'meona_curve', 'patient_id'
    ];

    /**
     * Beziehung zwischen Case und Patient wird definiert
     */
    public function patient()
    {
    	return $this->belongsTo(Patient::class);
    }

    /**
     * Beziehung zwischen Case und Departement wird definiert
     */
    public function department()
    {
    	return $this->belongsTo(Department::class);
    }

    /**
     * Beziehung zwischen Case und Bed wird definiert
     */
    public function bed()
    {
        return $this->hasOne(Bed::class);
    }
}
