<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
	/**
	 * Gibt an welche Attribute von Userinputs eingetragen werden dürfen
	 * 
	 * @var array
	 */
    protected $fillable = [
    	'name', 'date', 'number_of_beds', 'slug'
    ];

    /**
     * Funktion von https://laracasts.com/discuss/channels/laravel/how-to-create-url-slugs-in-laravel
     *
     * Erstellt beim Erstellen des Models ein URL-freundliche Slug mit dem Namen des Models
     *
     * Slug werden für Route Model Binding gebraucht
     * 
     * @static
     */
    public static function boot()
    {
    	parent::boot();

    	static::saving(function ($model) {
    		$model->slug = str_before(str_slug($model->name), '-');
    	});
    }

    /**
     * Die Controller suchen jetzt den Slug dieses Models und nich mehr den Primary Key id,
     * wenn in der URL das Model vorkommt.
     * 
     * 
     * @return string
     */
    public function getRouteKeyName()
    {
    	return 'slug';
    }

    /**
     * Die Beziehung zwischen Day und Bed wird definiert.
     */
    public function beds()
    {
    	return $this->hasMany(Bed::class);
    }

    /**
     * Filter um den gesuchten Tag zu erhalten
     *
     * @param Illuminate\Database\Eloquent\Builder
     * @return  Illuminate\Database\Eloquent\Builder
     */
    public function scopeRequestedDay($query, $date)
    {
        $query->where('slug', $date);
    }
}
