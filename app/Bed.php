<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Bed extends Model
{
    /**
     * Gibt an welche Attribute von Userinputs eingetragen werden dürfen
     *
     * @var array
     */
    protected $fillable = [
        'day_id', 'patient_case_id', 'department_id', 'type'
    ];

    /**
     * Sortiert alle Betten nach der Abteilung
     * @static
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function(Builder $builder) {
            $builder->orderBy('department_id', 'asc');
        });
    }

    /**
     * Beziehung zwischen Bed und Day wird definiert.
     */
    public function day()
    {
    	return $this->belongsTo(Day::class);
    }

    /**
     * Beziehung zwischen Bed und Departement wird definiert
     */
    public function department()
    {
    	return $this->belongsTo(Department::class);
    }

    /**
     * Beziehung zwischen Bed und Case wird definiert
     */
    public function patientCase()
    {
    	return $this->belongsTo(PatientCase::class);
    }

    /**
     * Filter um alle Betten des gesuchten Tages zu bekommen
     * 
     * @param Illuminate\Database\Eloquent\Builder
     * @param App\Day
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfDay($query, $day)
    {
    	return $query->where('day_id', $day->id);
    }

    /**
     * Filter um alle Betten auf der Aktiven Liste zu bekommen
     *
     * @param Illuminate\Database\Eloquent\Builder
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('type', 'aktiv');
    }

    /**
     * Filter um alle Betten auf der Warteliste zu bekommen
     * 
     * @param Illuminate\Database\Eloquent\Builder
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeWaitlist($query)
    {
        return $query->where('type', 'warteliste');
    }

    /**
     * Filter um alle Betten auf der Abmeldeliste zu bekommen
     *
     * @param Illuminate\Database\Eloquent\Builder
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeSignoutlist($query)
    {
        return $query->where('type', 'abmeldung');
    }

    /**
     * Filter um alle Betten zu bekommen, die keinen Fall haben
     * 
     * @param Illuminate\Database\Eloquent\Builder
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasNoPatientCase($query)
    {
        return $query->where('patient_case_id', null);
    }

    /**
     * Filter um alle Betten einer Abteilung zu bekommen.
     *
     * @param Illuminate\Database\Eloquent\Builder
     * @param int
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfDepartment($query, $department)
    {
        return $query->where('department_id', $department);
    }

    /**
     * Filter um alle Betten mit einem Fall zu bekommen
     *
     * @param Illuminate\Database\Eloquent\Builder
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function scopeHasPatientCase($query)
    {
        return $query->where('patient_case_id', '!=', null);
    }
    
}
