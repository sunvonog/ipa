<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	/**
	 * Hier wird die Beziehung zum User umgekehrt.
	 * Die Rolle gehört zu vielen Usern.
	 *
	 * Wenn man die Funktion aufruft, bekommt man alle Users der Rolle.
	 * 
	 */
    public function users()
    {
    	return $this->hasMany(User::class);
    }
}
