<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
	/**
	 * Gibt an welche Attribute von Userinputs eingetragen werden dürfen
	 * 
	 * @var array
	 */
    protected $fillable = [
    	'name', 'firstname', 'sex', 'birthdate'
    ];

    /**
     * Beziehung zwischen Patient und Case wird definiert
     */
    public function patientCases()
    {
    	return $this->hasMany(PatientCase::class);
    }
}
