<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    /**
	 * Beziehung zwischen Departement und Bed wird definiert
	 */
    public function beds()
    {
    	return $this->hasMany(Bed::class);
    }

    /**
     * Beziehung zwischen Departement und PatientCase wird definiert.
     */
    public function patientCase()
    {
    	return $this->hasMany(PatientCase::class);
    }
}
