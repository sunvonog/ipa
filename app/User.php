<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Builder;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Gibt an welche Attribute von Userinputs eingetragen werden dürfen
     *
     * @var array
     */
    protected $fillable = [
        'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Bei jeder Abfrage nach dem User wird werden die User nach created_at absteigend sotiert.
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function(Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }

    /**
     * Erstellt das Attribut firstname
     * @return string
     */
    public function getFirstnameAttribute()
    {
        return str_after($this->name,  ' '); 
    }

    /**
     * Erstellt das Attribut lastname
     * @return string
     */
    public function getLastnameAttribute()
    {
        return str_before($this->name, ' ');
    }

    /**
     * Hier wird die Beziehung zwischen dem Model User und dem Model Role definiert.
     * Sobald man die Funktion aufruft, bekommt man die Rolle des Users
     *  
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Checkt ob der User die erforderte Rolle hat
     * @param  string
     * @return boolean   
     */
    public function hasRole($role)
    {
        return $role == $this->role_id;
    }
}
